TAB Charter
===========

The primary purpose of The Linux Foundation Technical Advisory Board (TAB) is
to advise The Linux Foundation Board of Directors (Board) and the management of
The Linux Foundation (Management) on matters related to supporting the
technical agenda of The Linux Foundation. The following main areas are included
in this charter:

1. Oversee specific programmes which are identified by mutual agreement between
the Board, the Management and the TAB.
2. Provide a formal, twice yearly report to the Board on the status of and
progress made in all areas under the purview of the TAB.
3. Promote and Support the programmes with which the TAB is charged to the
wider Linux and Open Source Communities.

Election Procedures
-------------------
1. The election shall be held at the same time as the Linux Kernel Summit.
2. The elections shall be run by a commitee consisting of TAB members who
are not up for election.
3. If the Linux Kernel Summit will not be held in a calendar year the election
committee shall announce a date for elections with at least one month notice.
4. Active kernel community members shall be eligible to vote. The entire TAB
will decide what criteria makes up an active kernel community member. The
election committee will announce those specific requirements for voting at
least one month before the date of the elections.
5. Self nominations for the election shall be accepted from any active kernel
community member, via email to the designated election mailing list, up until
the time of the election.

Meetings and Membership
-----------------------

1. The TAB consists of ten voting members.
2. Membership of the TAB shall be for a term of 2 years with staggered 1-year
elections.
3. The TAB shall elect a Chair and Vice-Chair of the TAB from amongst their
members to serve a renewable 1 year term.
4. The Chair shall be Nominated for a Voting position on the Board with the
Vice-Chair serving as his Observer.
5. The Chair or Vice-Chair shall prepare an agenda for and preside over monthly
meetings of the TAB.
6. A member of the TAB may be removed by a resolution of the TAB supported by
more than two thirds of its voting membership.
7. The TAB may fill any vacancy arising by removal or resignation by a simple
majority vote to fill the remainder of the term of the vacating member.
8. The rules of election and membership outlined in this section may be varied
by a resolution of the TAB supported by more than two thirds of its voting
membership.

Operation
---------
1. The TAB is authorized to seek advice and counsel from other interested
parties and invited experts as appropriate.
2. Any outside party wishing to bring an issue before the TAB may do so by
emailing the TAB mailing list
3. The TAB shall provide transparent and timely reporting (through any
mechanism it deems appropriate) to the Community at large on all of its
activities subject to the right of any individual to designate their comments
and the ensuing discussion as “in confidence” in which case the public report
shall contain only a note of the request and an agreed summary (if any) of the
substance.
4. The TAB is being formed at the discretion of the Board. The Board alone may
decide to terminate the TAB in its sole discretion; provided however, that the
Board or its authorized officer shall first consult the TAB Chair.
5. As an entity under the auspices of The Linux Foundation, the TAB and its
members shall abide by The Linux Foundation's policies applicable to non-member
participants, including the Antitrust Policy, posted on The Linux Foundation's
website.
