Minutes - Jan. 2nd, 2020
========================

**Roll call:** Steve, Greg, Laura, Olof, Jon, Chris, Dan, Ted, Sasha

LF Board Meeting Summary
------------------------

Chris summarized the LF board meeting from Nov 2019.

TAB Elections
-------------

We reviewed Laura’s proposed rules, which enable electronic voting for some active contributors that are not able to attend the kernel summit or attached conferences.


Code of Conduct Report
----------------------

This was published, no additional followups required

Plumbers
--------

This month’s plumber’s update was focused on making the PC proposal format more useful to both the PC and the TAB.  The main benefits that we’d like to retain from the current format are to communicate an overall strategy for the conference logistics and mission, as well as giving the TAB a way to make sure the Plumber’s PC has all of the staffing that it needs.

The existing format expected multiple PCs to submit bids for plumbers, but over the past few years we’ve ended up with an explicit handoff from one PC to another.  Since we expect this to continue, we’re likely to drop the bidding process for now.

Kernel workflows
----------------

Checking in on the overall status, planning is still on-going and the TAB is happy to help where we can.
