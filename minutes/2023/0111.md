Minutes - January 11th, 2023
============================

Roll Call
---------

Dan, Christian, Dave, Greg, Jakub, Jon, Kees, Steven

Approving December Minutes
--------------------------

Approved

Patchwork project funding
-------------------------

Discussed potential funding sources

Plumbers
--------

Update on location finalization

GTI
---

Continuing to monitor

LFX Collaboration
-----------------

Invite Nirav Patel (CTO of LF) to February meeting to introduce LFX

Open source contribution maturity model
---------------------------------------

Upstream discussion proceeding

Maintainer helpline / collaborative mediation
---------------------------------------------

Discussed next steps for developing mediation support for maintainers

