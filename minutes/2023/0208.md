Minutes - February 8th, 2023
============================

Roll Call
---------

Christian, Dave, Jakub, Jon, Sasha, Greg, Kees, Dan (absent: Ted, Steven)

Guests: Nirav, Nina

Approving January Minutes
-------------------------

Approved

Opens
-----

None

LFX and the Linux Kernel
------------------------

Guest Nirav Patel CTO of the Linux Foundation presented the current state of LFX and the opportunities and challenges concerning kernel community usage of the infrastructure
.

Plumbers
--------

Christian provided an update on Plumbers planning

Maintainer helpline / collaborative mediation update
----------------------------------------------------

Dave discussed learnings from the Intel mediation program

GTI
---

Noted that there were no new developments to discuss

Open source contribution maturity model
----------------------------------------

Discussed next steps

