Minutes - September 13th, 2023
==============================

Roll Call
---------

Dan
Kees
Sasha
Steven
Ted
Dave
Jon
(Christian)
(Greg)
(Jakub)

Approving July Minutes
----------------------

Approved

TAB Elections
-------------

Discussed preparations for next election
Finalize the ballot notification and election announce mail at next meeting

Plumbers
--------

Discussed the state of Plumbers attendance and waitlist management

Communication Style / Conflict Resolution
-----------------------------------------

Discussed a draft of the Communication Style document and an open discussion on conflict resolution

