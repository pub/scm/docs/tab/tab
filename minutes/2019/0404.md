Minutes - Apr. 4th, 2019
========================

**Attendees:** Chris, Steve, Ted, Kees, Laura, Tim, Jon, Greg, Ted
**Special Guest:** Shuah Khan

LF Community Bridge program
---------------------------
Shuah Khan joined the call to provide details about the Linux Foundation's new Community Bridge program, which includes a mentorship program to work on open source projects.  The program allows developers and mentors and sponsoring organizations to connect, and provide funding for projects in the kernel and elsewhere.  Shuah described the program, and the current status of proposals.  Over 200 mentees have registered, and there are a few projects already planned.  Shuah is looking to recruit more mentors.  The Linux Foundation will immediately fund 5 projects related to the Linux kernel, and will match funding for the first 100 mentees.  More details will be provided about the pool of projects and areas of focus.  Shuah asked if the TAB could help get the word out to candidate mentors in the kernel space.

Minutes review
--------------

The minutes from the March meeting had one issue that needed correction, and then a vote will be held on the mailing list to get approval for public distribution.

Setting up CoC committee
------------------------

There was more discussion about possible candidates for the CoC committee, but work continues on setting up that committee.  Olof was not available for the call, so this was mostly deferred.

TAB election voting process
---------------------------

Laura researched additional voting systems, but did not find any new candidates.  Tim researched modifying the condorcet system to use a simple (non-condorcet) tabulation method (to match how we've tabulated TAB election votes in the past).  It's easy to change the tabulation method, but the user interface to the voting system would be somewhat difficult to change from a ranking design.  More research was needed, and Chris will work on resolving where the voting system will be hosted.

Plumbers
--------

Things are on track for 2019, with discussions for 2020 planning starting in May.  It looks like the vice-chair for this year's Plumbers Conference (Laura) will become the Chair for 2020.  A proposal was made for a TAB panel for OSSNA, which will likely be accepted.  Sufficient TAB members will be there for this to proceed.

TAB chair and vice-chair election
---------------------------------

The election for TAB chair and vice-chair was held during the call, and Chris was re-elected as TAB Chair, and Jon as TAB Vice-Chair.
