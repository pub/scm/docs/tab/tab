Minutes - June 6th, 2019
========================

**Attendees:** Chris, Steve, Kees, Laura, Tim, Jon, Greg, Ted, Olof, Dan

Minutes review
--------------

The minutes from the May meeting were approved.
 
Setting up the Code of Conduct committee
----------------------------------------

Shauh Khan and Kristen Acardi accepted invitations to be on the Code of Conduct committee, which now consists of Greg, Mishi, Shuah and Kristen.  The web site will be updated to reflect the committee members.  Greg is also working on the first summary report of issues handled by the committee.

TAB election voting process
---------------------------

There was continuing discussion about possible changes to the TAB election process.  Laura Abbott is preparing a statement announcing the proposed changes for this year.  To avoid possible unanticipated consequences, not all of the changes under discussion may be implemented this year.

Plumbers
--------

Work continues on preparations for Linux Plumbers, scheduled for September in Lisbon Portugal.  Micro-conference proposals are getting reviewed and approved.  Not all micro-conferences will be approved.  However, people are delaying reserving their hotel room, which may be an issue.
