Minutes - Mar. 7th, 2019
========================

**Attendees:** Chris, Greg, Jon, Tim, Ted, Steve, Dan, Laura, Kees
**Apologies:** Olof

Minutes approval
----------------

Unanimous approval for last two meetings' notes.

Code of conduct committee docs and action items
-----------------------------------------------

Olof continuing to make edits. Should be done within the next week or so. CoC committee member selection continued to be discussed. Committee will be 5 members including Greg KH and Mishi Choudhary. It is expected that the TAB will reach out to specific individuals to fill the remaining 3 slots with the goal of finding responsible people in the kernel community who do not overlap with TAB.

Picking up the electronic voting discussion
-------------------------------------------

Sample electronic voting (condorcet) finished, and looks promising. However, switching tally method AND casting method at the same time may not be the best idea. Research will continue to try to find a non-ranked electronic voting system so we can reach the more important goal of allowing "absentee voting" for TAB elections. Criteria for absentee voting is still under discussion.

Scheduling elections for chair/vice chair
-----------------------------------------

Vote will happen at April meeting. Possibly with new electronic voting system.

Plumbers news
-------------

No major news. Progress continues, announcement is expected before the next TAB meeting.

Maintenance project funding
---------------------------

With other project funding mechanisms aimed mostly at novel projects or entry-level contributors, there needs to be a place to fund specific ongoing kernel projects that do not have another home. Discussion with LF is expected to help resolve this.
