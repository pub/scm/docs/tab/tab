Minutes - September 7th, 2021
========================

TAB elections
-------------

Discussed calls for nominations
Discussed pending changes to the elections/membership section of the charter.
Discussed reminder emails to make sure people vote/participate.

Plumbers
--------

Discussed the final round of preparations and microconf details.
Discussed LPC 2022.

