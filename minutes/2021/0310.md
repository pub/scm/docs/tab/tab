Minutes - March 10th, 2021
==========================

Chair and Vice Chair Elections
------------------------------

Chair: Chris Mason

Vice Chair: Steve Rostedt

LF Board Meeting Notes
----------------------

Chris brought up notes from the Linux Foundation Board meeting.

Plumbers update
---------------

Microconf CFP nearly ready

Discussed in-person vs hybrid event strategies.

LSF/MM targeting first week in December in Palm Springs.

Kernel workflows
----------------

vger backlog is clearing

