Minutes - July 14th, 2021
========================

Approving May Minutes
---------------------

Approved

Approving June Minutes
----------------------

Approved

UMN followup
------------

Discussed followups about security researchers and the kernel mailing lists.

TAB elections thread
--------------------

Discussed potential clarifications to the election procedures in the TAB charter.
Discussed metrics and cutoffs for automatic ballot emails.
Discussed dates for public emails about voting procedure.

Plumbers
--------

Discussed program side (content) and the technical side (updates to BBB etc).  Everything is on track.

