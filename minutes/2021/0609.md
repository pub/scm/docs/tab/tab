Minutes - June 9th, 2021
========================

BBB testing
-----------

Plumbers will use BBB for video conferencing again this year.

Discussed different topics for a BBB load test.  The goal is testing both the streaming capacity and ability to withstand the load from many people participating in the discussion.  We suggested:
- kernel workflow update
- beer hall talk

UMN Followup
------------

Discussed followup plans for a best practices document.

TAB elections and related charter changes
-----------------------------------------

Discussed changes to the election procedure allowing fully remote voting.

Plumbers
--------

Discussed microconfs accepted and pending.

Looking for more submissions to the refereed track.

Discussed cap on attendance.

Discussed plans for 2022.

