Minutes - February 9th, 2022
============================

Roll call
---------

Chris, Greg, Jon, Kees, Laura, Sasha, Steve, Ted, Dan

Minutes
-------

Approving January Minutes: (not yet)

Approving December Minutes: Approved

Chair and vice-chair nominations
--------------------------------

Chris Mason stands for chair again, and opened the floor for additional nominations.

Maintainers summit follow up
----------------------------

Continued discussion of challenges maintainers face and how to encourage more open source support from employers.
Security research best practices
Kees is working on documentation around best practices for security researchers and their community engagements.
Vger problems delivering to gmail
Vger lists continue having trouble reliably delivering to gmail, which causes large backlogs and other problems.  We’re engaging with google to try and improve things, but haven’t had much traction yet.

Plumbers
--------

Website is up and running, along with 2022 CFP.

