Minutes - November 9th, 2022
============================

Roll Call
---------
Greg, Jon, Kees, Sasha, Ted, Steve, Dan, Dave, Jakub

Approving August Minutes
------------------------

Approved

Approving October Minutes
-------------------------

Approved

Plumbers
--------

Tech team post mortem 
BBB for other conferences
Update on the location search

Opensource contribution maturity model
--------------------------------------

Discussed the roll out plan and trailers for the document

GNU Toolchain Infrastructure
----------------------------

Discussed the ongoing debate in the community

Maintainer Help Line
--------------------

Talked through the idea of having more formalized support for maintainers and conflict management.

Debian
------

Discussed a potential funding opportunity

