Minutes - January 10th, 2024
============================

Roll Call
---------

Kees
Sasha
Steven
Ted
Dan
Dave
Jon
(Christian)
Greg
Jakub

Approving December Minutes
--------------------------

Approved

2024 Charter Updates
--------------------

Discussed potential election scheduling updates to increase participation.
Proposal to shift election to end of Plumbers/Kernel Summit week. Allow for some in person encouragement to others to run.
https://git.kernel.org/pub/scm/docs/tab/tab.git/tree/documents/charter.md

2024 TAB Projects
-----------------

TAB Participation: Discussed opportunities to increase transparency and engagement in TAB activities
Management Style: Discussed cleaning up Management Style to prepare it for further updates
Tech Writer: Follow up discussion from Kernel Summit about sponsoring a tech writer for kernel documentation
Workflows Support: Discussed importance of and promotion of the lore, lei and b4 projects

Plumbers Update
---------------

Discussed next steps for 2024 Plumbers

Kernel CVE Reporting
--------------------

Discussed potential changes for how Kernel CVEs are managed

