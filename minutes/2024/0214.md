Minutes - February 14th, 2024
=============================

Roll Call
---------

(Christian)
Dan
Dave
(Greg)
Jakub
Jon
Kees
Sasha
Steven
Ted

Approving January Minutes
-------------------------

Approved

Chair and Vice Chair Elections
------------------------------

Resolved to run a CIVS poll with nominations due before the next TAB meeting and poll to close at the meeting.

Plumbers update
---------------

Discussed next steps for Plumbers including CFP and deadlines for microconferences

2024 TAB Projects Follow Up
---------------------------

Maintainer Support
Management Style
Tech Writer
Workflows support
Maturity Model

